package controller

import (
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gopkg.in/pg.v5"
)

type Handler struct {
	DB *pg.DB
}

type message struct {
	ID      int64  `json:"id"`
	Message string `json:"message"`
}

func (h *Handler) GetMessage(c echo.Context) error {
	var msg message
	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return c.String(http.StatusBadRequest, "Parameter id is invalid")
	}

	err = h.DB.Model(&msg).Where("id = ?", id).First()

	if err != nil {
		return c.String(http.StatusNotFound, "Not found")
	}

	return c.String(http.StatusOK, msg.Message)
}

func (h *Handler) PostMessage(c echo.Context) error {
	req := c.Request()
	bytes, err := ioutil.ReadAll(req.Body)

	if err != nil {
		return c.String(http.StatusInternalServerError, "Internal server error")
	}

	body := string(bytes)

	if len(body) == 0 {
		return c.String(http.StatusBadRequest, "Request body is empty")
	}

	tx, err := h.DB.Begin()

	if err != nil {
		return c.String(http.StatusInternalServerError, "Failed to establish transaction")
	}

	msg := message{
		Message: body,
	}

	err = tx.Insert(&msg)

	if err != nil {
		return c.String(http.StatusInternalServerError, "Failed to insert a message")
	}

	err = tx.Commit()

	if err != nil {
		return c.String(http.StatusInternalServerError, "Failed to commit a message")
	}

	return c.JSON(http.StatusOK, msg)
}
