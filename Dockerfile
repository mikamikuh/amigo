FROM golang:1.8.3-stretch

ENV APP_DIR /go/src/bitbucket.org/mikamikuh/amigo
RUN curl https://glide.sh/get | sh

WORKDIR $APP_DIR
COPY . .

RUN glide install
RUN go get gopkg.in/pg.v5
RUN go build

EXPOSE 1323
EXPOSE 1323
CMD ["/go/src/bitbucket.org/mikamikuh/amigo/amigo"]