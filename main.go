package main

import (
	"os"

	"bitbucket.org/mikamikuh/amigo/controller"

	"github.com/labstack/echo"
	"gopkg.in/pg.v5"
)

func initDB() *pg.DB {
	return pg.Connect(&pg.Options{
		User:     os.Getenv("DATABASE_USER"),
		Database: os.Getenv("DATABASE_NAME"),
		Addr:     os.Getenv("HOST"),
		Password: os.Getenv("PASSWORD"),
	})
}

func main() {
	e := echo.New()
	h := controller.Handler{
		DB: initDB(),
	}

	e.GET("/messages/:id", h.GetMessage)
	e.POST("/messages/", h.PostMessage)

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
