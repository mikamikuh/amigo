# How to run

## Database setup
PostgreSQL database works on Docker container and it mounts `.data` folder under the root directory.
```
$ docker-compose run postgres
$ psql -U postgres -h localhost -p 5432 -c 'CREATE DATABASE amigo;'
$ psql -U postgres -h localhost -p 5432 -d amigo -f migration.sql
```

## Run application
`docker-compose up` builds Docker image for a web application and runs the container and postgres container.

## Usage
`curl localhost:1323/messages/ -d 'my test message to store`
`curl localhost:1323/messages/1`